// webpack.config.js
var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('./public/build')
    .setPublicPath('/public/build')
    .addEntry('app', './public/build/js/app.js')
    .addStyleEntry('global', './public/build/css/app.css')
    .enableSourceMaps(!Encore.isProduction())
;

// build the first configuration
const adminConfig = Encore.getWebpackConfig();

// Set a name for the config (don't forget that line!)
adminConfig.name = 'adminConfig';

// Reset Encore to build the second config
//Encore.reset();

// export the final configuration
module.exports = [adminConfig];