<?php

namespace App\Controller\WebApi\V1;

use Symfony\Component\Routing\Annotation\Route;
use App\Controller\Rest\V1\ApiController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/apiweb/v1")
 * @Security("has_role('ROLE_ADMIN')")
 */
class ApiWebController extends ApiController
{
}
