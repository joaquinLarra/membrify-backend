<?php

namespace App\Controller\Rest\V1;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use FOS\RestBundle\View\View;

/**
 * @Route("/v1")
 * @Security("has_role('ROLE_USER')")
 */
class ApiController extends FOSRestController
{

    /**
     * API Constructor
     */
    public function __construct()
    {
    }

    /**
     * @Rest\Get("/ping")
     * @Rest\Post("/ping")
     */
    public function ping()
    {
        return new JsonResponse(array("ping" => "pong"), 200);
    }
    
 
    /**
     * Create User
     * @Rest\Post("/user")
     */
    public function createUser(Request $request)
    {
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());
            if ($request->request->has('username') && $request->request->has('pass') && $request->request->has('email')) {
                $array = $this->get('SecurityController')->createUser($request->request->get('username'), $request->request->get('pass'), $request->request->get('email'));
                return View::create(array("message" => "good"), Response::HTTP_OK);
            }
        }
        return new JsonResponse(array("error" => "Missing Parameters"), 400);
    }
}
