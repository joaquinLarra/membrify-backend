<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class SecurityController extends Controller
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoderInterface;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Constructor
     *
     * @param UserPasswordEncoderInterface $userPasswordEncoderInterface
     * @param EntityManagerInterface $entityManager
     */
    function __construct(UserPasswordEncoderInterface $userPasswordEncoderInterface, EntityManagerInterface $entityManager)
    {
        $this->userPasswordEncoderInterface = $userPasswordEncoderInterface;
        $this->entityManager = $entityManager;
    }

    // /**
    //  * @Route("/login", name="login")
    //  */
    // public function login(Request $request, AuthenticationUtils $authenticationUtils)
    // {
    //     // get the login error if there is one
    //     $error = $authenticationUtils->getLastAuthenticationError();
    
    //     // last username entered by the user
    //     $lastUsername = $authenticationUtils->getLastUsername();
    
    //     return $this->render('security/login.html.twig', array(
    //         'last_username' => $lastUsername,
    //         'error'         => $error,
    //     ));
    // }

    // /**
    //  * @Route("/jsonlogin", name="json_login")
    //  */
    // public function jsonLogin(Request $request)
    // {
    //     return new JsonResponse(array("success" => "login"), 200);
    // }

    // /**
    //  * @Route("/register", name="register")
    //  */
    // public function register(UserPasswordEncoderInterface $encoder)
    // {
    //     // whatever *your* User object is
    //     $user = new User();
    //     $plainPassword = '';
    //     $encoded = $encoder->encodePassword($user, $plainPassword);

    //     // $user->setPassword($encoded);

    //     return $this->render('security/login.html.twig', array(
    //         'last_username' => $encoded,
    //         'error'         => null,
    //     ));
    // }

    public function createUser($username, $plainPassword, $email, $isAdmin = 0)
    {
        // whatever *your* User object is
        $user = new User();
        $encoded = $this->userPasswordEncoderInterface->encodePassword($user, $plainPassword);
        $user->setUsername($username);
        $user->setPassword($encoded);
        $user->setEmail($email);
        $user->setIsActive(1);
        $user->setIsAdmin($isAdmin);
        $user->setApiKey(Uuid::uuid1());

        $entityManager = $this->entityManager;
        $entityManager->persist($user);
        $entityManager->flush();

        return true;
    }
}
