<?php
namespace App\Controller;

use AlterPHP\EasyAdminExtensionBundle\Controller\AdminController as BaseAdminController;


use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class AdminController extends BaseAdminController
{
    protected function initialize($request)
    {
        $this->get('translator')->setLocale('es');
        parent::initialize($request);
    }
    
}