<?php
/**
 * see http://symfony.com/doc/current/security/guard_authentication.html
 */

namespace App\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class TokenAuthenticator extends AbstractGuardAuthenticator
{
    private $security;
    private $authChecker;


    public function supports(Request $request)
    {
        if ($request->headers->has('X-AUTH-API-USER') && $request->headers->has('X-AUTH-API-KEY')) { 
            return true;
        }
        return false;
    }

    public function getCredentials(Request $request)
    {
        return array(
            'apiUser' => $request->headers->get('X-AUTH-API-USER'),
            'apiKey' => $request->headers->get('X-AUTH-API-KEY'),
        );
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $apiUser = $credentials['apiUser'];
        $apiKey = $credentials['apiKey'];

        if (null === $apiUser && null === $apiKey) {
            return;
        }

        // if a User object, checkCredentials() is called
        return $userProvider->loadUserByUsername($apiUser);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        // check credentials - e.g. make sure the password is valid
        $apiUser = $credentials['apiUser'];
        $apiKey = $credentials['apiKey'];

        if (trim($apiUser) === $user->getUserName() && trim($apiKey) == $user->getApiKey()) {
            
            return true;
        }

        return false;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = array(
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        );

        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // on success, let the request continue
        return null;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = array(
            // you might translate this message
            'message' => 'Authentication Required'
        );

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
